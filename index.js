const aws = require('aws-sdk');

const assumeRole = async () => {
    const STS = aws.STS;
    try {
        const sts = new STS();
        const data = await sts
            .assumeRole({
                RoleArn: process.env.AWS_ROLE_S3,
                RoleSessionName: 'sdk-aws',
            })
            .promise();
        if (!data.Credentials) {
            throw new Error('Invalid credentials');
        }
        const roleCredentials = {
            accessKeyId: data.Credentials.AccessKeyId,
            secretAccessKey: data.Credentials.SecretAccessKey,
            sessionToken: data.Credentials.SessionToken,
        };
        console.log('TOKEN GENERATED');
        console.log('TOKEN GENERATED', roleCredentials);
        return roleCredentials;
    } catch (error) {
        console.log(error.message);
        throw new Error(error);
    }
};

const getDataFromS3 = async () => {
    console.log('AWS_ROLE_S3 a asumir', process.env.AWS_ROLE_S3);
    console.log('REGION', process.env.REGION);
    console.log('AWS_S3_NAME', process.env.AWS_S3_NAME);
    const roleCredentials = await assumeRole();
    const s3 = new aws.S3({
        region: process.env.REGION,
        ...roleCredentials,
    });
    const params = {Bucket: process.env.AWS_S3_NAME};
    const response = await s3.listObjectsV2(params).promise();
    console.log('Respuesta: ', response.Contents)
};

exports.handler = async (event) => {
    // TODO implement
    console.log(event);
    await getDataFromS3();
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello NotPinkCon! from Lambda - poc- lambda!'),
    };
    return response;
};
